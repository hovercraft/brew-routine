# brew-routine
Auto-updating of homebrew formulae and casks

### Installation
Link the brew-routine command to usr/local/bin:

```bash
ln -s /<full>/<path>/<to>/brew-routine.sh /usr/local/bin/brew-routine
```

### Cask Configuration
Specify the casks you want to upgrade with this tool line by line in upgrade-casks.txt. Lines beginning with '#' will be ignored. Use the prefix 'pw:' to flag casks that require a password for upgrading (e.g. java).

### Manual Execution from Terminal
Use the command 'brew routine' to upgrade your tasks manually from the terminal. The following flags are available:
*  `--silent`, `-s`: Reduces console output to a minimum
* `--skipdoctor`: Skips the execution of `brew doctor`
* `--skipsudo`: Skips the upgrade of casks which would require a password for upgrading. These have to be marked with the prefix 'pw:' in the cask configuration file.
* `--forcelatest`: Forces the upgrade of cask with version 'latest', which would otherwise be skipped.
*  `--version`, `-v`: Prints the version of this script, then exits.

### LaunchAgent
If you want to execute brew-routine automatically on a regular basis, use the provided LaunchAgent (org.hovercraft.brewroutine.plist). Customize it (especially the path to the application) and place it in ~Library/LaunchAgents.

### Homebrew Copyright Notice
homebrew and the homebrew logo are copyright of the Homebrew contributors

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
- Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#!/bin/zsh

# Version number of this script
VERSION="brew-routine 1.4.5"
# Absolute path to this script, e.g. /home/user/bin/foo.sh
SCRIPT=$(readlink "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
# Path of the log file
LOGFILE="$SCRIPTPATH/brew-routine-log.txt"
# Path of the file where the list of casks to upgrade is saved
UPGRADE_CASKS_FILE="$SCRIPTPATH/upgrade-casks.txt"

# Identify flags (silent mode, skip doctor & skip sudo)
SILENT_MODE=false
SKIP_DOCTOR=false
SKIP_SUDO=false
FORCE_LATEST=false
for flag in "$@"
do
    if [[ $flag == -* ]]; then
        if [[ "$flag" == "-v" || "$flag" == "--version" ]]; then
			echo $VERSION
			exit
		elif [[ "$flag" == "-s" || "$flag" == "--silent" ]]; then
            SILENT_MODE=true
        elif [[ "$flag" == "--skipdoctor" ]]; then
            SKIP_DOCTOR=true
        elif [[ "$flag" == "--skipsudo" ]]; then
            SKIP_SUDO=true
        elif [[ "$flag" == "--forcelatest" ]]; then
            FORCE_LATEST=true
        else
            echo "Invalid option $flag"
            echo "Only --forcelatest, --silent/-s, --skipdoctor, --skipsudo, and --version/-v are allowed"
            exit
        fi
    fi
done

if [[ "$SILENT_MODE" == true ]]; then
    del=$'\n'"---------------------------------------------"$'\n'"$(date "+%Y-%m-%d %H:%M:%S")"$'\n'
    echo "$del" >> $LOGFILE 2>&1
fi

# Step 1: Check internet connection
ping -q -c 1 -W 1 github.com &> /dev/null
if ! [[ $? -eq 0 ]]; then
    if [[ "$SILENT_MODE" == true ]]; then
        echo "BrewRoutine Not Performed"
        echo "No internet connection!" >> $LOGFILE 2>&1
    fi
    echo "No internet connection!"
    exit
fi

# Step 2: Check if homebrew is available
if ! command -v brew &> /dev/null
then
    if [[ "$SILENT_MODE" == true ]]; then
        echo "BrewRoutine Not Performed"
        echo "Homebrew installation could not be found." >> $LOGFILE 2>&1
    fi
    echo "Homebrew installation could not be found"
    exit
fi
    
# Step 3: Update homebrew itself & check its working
if [[ "$SILENT_MODE" == true ]]; then
    brew update >> $LOGFILE 2>&1
    if [[ "$SKIP_DOCTOR" == false ]]; then
        output="$(brew doctor 2>&1)"
        echo "$output" >> $LOGFILE 2>&1
        if [[ "$output" != "Your system is ready to brew." ]]; then 
            echo "Your system is not ready to brew"
			echo "Please run 'brew doctor' and resolve the issues"
            exit
        fi
    fi
else
    brew update
    if [[ "$SKIP_DOCTOR" == false ]]; then
        output="$(brew doctor)"
        echo "$output"

        if [[ "$output" != "Your system is ready to brew." ]]; then
            while true; do
                printf "Continue? (Y/n) "
                read answer
                answer=${(L)answer} # to lower case
                if [[ "$answer" == "y" || "$answer" == "yes" ]]; then
                    break
                elif [[ "$answer" == "n" || "$answer" == "no" ]]; then
                    exit
                fi
            done
        fi
    fi
fi

# Step 4: Get current versions
declare -A cask_list
brew list --cask --versions | while read -r line; do
    current_cask=${line%% *}
    current_version=${line#* }
    cask_list[$current_cask]=$current_version
done

# Step 5: Determine casks to upgrade
upgrade_casks=""
while read line; do
    ## skip empty lines and comments
    current_cask="$(echo -e "${line}" | tr -d '[:space:]')"
    if [ -z "$current_cask" ] || [[ $current_cask == \#* ]]; then
        continue
    fi
    
    # skip casks flagged with 'pw:' if SKIP_SUDO is not active    
    if [[ $current_cask == pw:* ]]; then
        if [[ "$SKIP_SUDO" == true ]]; then
            continue
        fi
        current_cask=${current_cask#*:}
    fi
    
    # skip casks with the version number 'latest' if FORCE_LATEST is not active
    if [[ "$FORCE_LATEST" == false ]] && [[ $cask_list[$current_cask] == latest ]]; then
        continue
    fi
    
    upgrade_casks="$upgrade_casks $current_cask" # add current cask to upgrade string
done < $UPGRADE_CASKS_FILE

charclass="[^[:space:]/~:]"
warning_not_upgrading_pattern="^Warning: Not upgrading ($charclass+), the latest version is already installed$"

if [[ "$SILENT_MODE" == true ]]; then
    upgrade_counter=0
    upgraded_formulae_and_casks=""
    
    # Step 6: Upgrade all formulae
    output="$(brew upgrade 2>&1)"
    
    # Step 7: Upgrade casks selected in step 4
    upgrade_casks="brew upgrade --cask $upgrade_casks"
    output="$output"$'\n'"$(eval ${upgrade_casks} 2>&1)"
    
    # Step 8: Create 2-line silent mode output
    pattern1="^🍺[[:space:]]+($charclass+) was successfully upgraded!" # typically casks
    pattern2="^🍺[[:space:]]+(/$charclass+)+/($charclass+)/$charclass+:" # typically formulae
	cleaned_output=""
    while read line; do
        if [[ $line == 🍺* ]]; then
            ((upgrade_counter++))
            append=""
            if [[ $line =~ $pattern1 ]]; then
                append=$match[1] # first group in the pattern
            elif [[ $line =~ $pattern2 ]]; then
                append=$match[2] # second group in the pattern
            else
                append="<unknown>"
            fi
            if [[ $upgrade_counter == 1 ]]; then
                upgraded_formulae_and_casks="$append"
            else
                upgraded_formulae_and_casks="$upgraded_formulae_and_casks, $append"
            fi	
		elif [[ ! $line =~ $warning_not_upgrading_pattern ]]; then
			cleaned_output+="$line"$'\n'
        fi
    done <<< "$output"
    
    echo "$cleaned_output" >> $LOGFILE 2>&1
    
    if [[ $upgrade_counter == 0 ]]; then
        echo "Everything already up to date"
        echo "See you tommorow!"
    else
        echo "Upgraded $upgrade_counter formulae and casks"
        echo "$upgraded_formulae_and_casks"
    fi
else
    # Step 6: Upgrade all formulae
    brew upgrade
    
    # Step 7: Upgrade casks selected in step 4
    upgrade_casks="brew upgrade --cask $upgrade_casks"
    eval ${upgrade_casks} 2> >(grep -Ev --color=always "$warning_not_upgrading_pattern" >&2)
	    
    # Step 8 is skipped
fi

# Step 9: Cleanup
if [[ "$SILENT_MODE" == true ]]; then
    brew cleanup >> $LOGFILE 2>&1
else
    brew cleanup
fi
